
export default class VaultHandler {
	constructor(userData){
		this.userData = userData;
		this.startVaultResponse = null;
		this.vaultNextStepResponse = null;
		this.socket = null;
		
		/* EXTENSION EVENTS */
		
		chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			
			switch(request.type){
				//FROM CONTENT - Segnalo al server l'inizio di un vault
				case 'start_vault':
					console.log('extensions - vault - '+request.type);
					if(this.isSocketConnected()){
						this.socket.emit('start_vault', {
							tab_id : sender.tab.id,
							unique_id : request.data.unique_id,
							vairus_id : request.data.vairus_id
						});
					}
					this.startVaultResponse = sendResponse;
					return true;
				break;
				
				
				//FROM CONTENT - Segnalo al server il progresso di un vault
				case 'vault_next_step':
					console.log('extensions - vault - '+request.type);
					if(this.isSocketConnected()){
						this.socket.emit('vault_next_step', {
							tab_id : sender.tab.id,
							unique_id : request.data.unique_id,
							offset : request.data.offset
						});
					}
					this.vaultNextStepResponse = sendResponse;
					return true;
				break;
			}
		}.bind(this));
	}
	
	updateSocket(socket){
		this.socket = socket;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			
			switch(event){
				//SPAWN VAULT
				case 'spawn_vault':
					console.log('socket - vault - '+event);
					var current_time = new Date().getTime();
					
					var suggested = this.userData.vairus_list.reduce(function(current, vairus) {
						if(vairus.exhaustion > current_time) return current;
						if(current == null) return vairus;
						
						var current_stats = 0;
						var vairus_stats = 0;
						
						data.avaiable_stats.forEach(function(stat){
							current_stats+= current.current_stats[stat].total;
							vairus_stats+= vairus.current_stats[stat].total;
						})
						
						return (current_stats > vairus_stats) ? current : vairus;
					}, null)
					
					//Invio il messaggio
					chrome.tabs.sendMessage(data.tab_id, {
						type : 'spawn_vault',
						data : {  
							unique_id : data.unique_id, 
							difficulty : data.difficulty, 
							avaiable_stats : data.avaiable_stats, 
							suggested_vairus : suggested
						}
					});
				break;
				
				//START VAULT
				case 'start_vault':
					console.log('socket - vault - '+event);
					this.startVaultResponse(data);
				break;
				
				//VAULT DECAYED
				case 'vault_decayed':
					console.log('socket - vault - '+event);
					this.startVaultResponse(data);
				break;
				
				//VAULT NEXT STEP
				case 'vault_next_step':
					console.log('socket - vault - '+event);
					this.vaultNextStepResponse(data);
				break;
				
				//END VAULT
				case 'end_vault':
					console.log('socket - vault - '+event);
					this.userData.addBytes(data.rewards);
					this.userData.syncProfileData(['bytes']);
					this.vaultNextStepResponse(data);
				break;
			}
		}.bind(this));
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}
}