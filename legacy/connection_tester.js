import io from 'socket.io-client';
import axios from 'axios';

var server = 'http://localhost:3000';
// var server = 'http://167.172.42.73';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
var i = 5;

setInterval(function(){
	for(var j=1;j<=20;j++){
		if(i<1000){
			requestNewToken('Darwen'+i, 'dadawa');
			i++;
		}
	}
}, 1000)


function requestNewToken(user, pass){
	console.log(user);
	var data = new URLSearchParams();
	data.append('username', user);
	data.append('password', pass);
	
	axios.post( server+'/login', data)
		.then(function (response) {
			if(response.data.success){
				
				var jwt = response.data.message;
				console.log('token - '+jwt);
				
				connectSocket(jwt);
				
			}else{
				console.log('login error');
			}
		})
		.catch(function (error) {
			console.log('login error catch');
			console.log(error);
		});
}


/* SOCKET HANDLER */

function connectSocket(jwt){
	console.log('connecting socket');
		
	var socket = io.connect(server, {
		query : 'token=' + jwt,
		reconnection : false,
		transports: ['websocket'], 
		upgrade: false
	});
	
	
	/* CONNECTION HANDLER */
	
	socket.on('connect', (data) => {
		console.log('socket - connected');
	});
	
	socket.on('disconnect', (data) => {
		console.log('socket - disconnect');
	});
	
	socket.on('connect_error', (data, a, b) => {
		console.log('socket - connect_error');
		console.log(data);
		console.log(a);
		console.log(b);
	});
	
	socket.on('connect_failed', (data) => {
		console.log('socket - connect_failed');
	});
	
	socket.on('error', (data) => {
		console.log('socket - error');
		
	});
}
