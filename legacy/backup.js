import io from 'socket.io-client';
import axios from 'axios';
import Vairus from './classes/vairusClass.js';
import VairusType from './classes/vairusTypeClass.js';
import UserData from './background/userdata.js';
import CatchHandler from './background/catchhandler.js';
import StructureHandler from './background/structurehandler.js';
import MatrixHandler from './background/matrixhandler.js';

var browser = browser || chrome;
var version = '0.10.2';
var version_valid = true;
var jwt;
var username;
var password;
var socket;
var server = 'http://localhost:3000';
// var server = 'http://167.172.42.73';
var language = window.navigator.userLanguage || window.navigator.language;
var reconnect = true;
var reconnect_timer;
var reconnect_delay = 5000;
var dex = {};
var username;
var active = true;

var userData = new UserData();
var catchHandler = new CatchHandler(userData);
var structureHandler = new StructureHandler(userData);
var matrixHandler = new MatrixHandler(userData, structureHandler);

browser.storage.local.get(['vairus_jwt','vairus_user','vairus_pass','vairus_active'], function(result) {
	if(result.hasOwnProperty('vairus_jwt')) jwt = result.vairus_jwt;
	if(result.hasOwnProperty('vairus_user')) username = result.vairus_user;
	if(result.hasOwnProperty('vairus_pass')) password = result.vairus_pass;
	if(result.hasOwnProperty('vairus_active')) active = result.vairus_active;
	
	// Auto login
	if(typeof jwt !== 'undefined'){
		console.log('local token');
		connectSocket(jwt);
	}else{
		console.log('no local token');
		
		if(typeof username !== 'undefined' && typeof password !== 'undefined'){
			console.log('local login data');
			requestNewToken(username, password);
		}else{
			console.log('no local login data');
		}
	}
});

/* LOGIN HANDLER */

var login_lock = false;
function requestNewToken(user, pass, sendResponse = false){
	if(!login_lock){
		console.log('req token');
		
		login_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('username', user);
		data.append('password', pass);
		
		axios.post( server+'/login', data)
			.then(function (response) {
				if(response.data.success){
					console.log('got new token');
					
					username = user;
					password = pass;
					
					jwt = response.data.message
					browser.storage.local.set({
						vairus_jwt: jwt,
						vairus_user: user, 
						vairus_pass : pass
					});
					connectSocket(jwt, sendResponse);
					
				}else{
					if(sendResponse){
						sendResponse({
							success: false,
							message : response.data.message
						});
					}
				}
				login_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				sendResponse({
					success: false,
					message : 'Server offline'
				});
				login_lock = false;
			});
	}
}

/* REGISTER HANDLER */

var register_lock = false;
function registerNewUser(username, password, email, sendResponse = false){
	console.log(username, password, email);
	if(!register_lock){
		console.log('register user');
		
		register_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('username', username);
		data.append('password', password);
		data.append('email', email);
		
		axios.post( server+'/register', data)
			.then(function (response) {
				console.log(response);
				if(response.data.success){
					requestNewToken(username, password, sendResponse)
				}else{
					if(sendResponse){
						sendResponse({
							success: false,
							message : response.data.message
						});
					}
				}
				register_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				sendResponse({
					success: false,
					message : 'Server offline'
				});
				register_lock = false;
			});
	}	
}


/* REQUEST RESET PASSWORD */

var request_reset_lock = false;
function requestResetPassword(email){
	
	if(!request_reset_lock){
		console.log('request reset password');
		
		request_reset_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('email', email);
		
		axios.post( server+'/request_reset', data)
			.then(function (response) {
				console.log(response);
				request_reset_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				request_reset_lock = false;
			});
	}	
}
/* RESET PASSWORD */

var reset_lock = false;
function resetPassword(code, password, sendResponse){
	
	if(!reset_lock){
		console.log('reset password');
		
		reset_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('code', code);
		data.append('password', password);
		
		axios.post( server+'/reset', data)
			.then(function (response) {
				console.log(response);
				sendResponse(response.data);
				reset_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				reset_lock = false;
			});
	}	
}


/* SOCKET HANDLER */

function connectSocket(jwt, sendResponse = false){
	if(!isSocketConnected()){
		console.log('connecting socket');
			
		socket = io.connect(server, {
			query : 'token=' + jwt,
			reconnection : false,
			transports: ['websocket'], 
			upgrade: false
		});
		
		
		/* CONNECTION HANDLER */
		
		socket.on('connect', (data) => {
			if(sendResponse){
				sendResponse({
					success: true,
					message : username
				});
			}
			
			userData.updateSocket(socket);
			catchHandler.updateSocket(socket);
			structureHandler.updateSocket(socket);
			matrixHandler.updateSocket(socket);
			
			socket.emit('check_version',{ version : version});
			socket.emit('get_userdata');
		
			//Resetto eventuai timer di riconnessione e setto il delay al minimo
			clearTimeout(reconnect_timer);
			reconnect_delay = 5000;
			changeFavicon();
			
		});
		
		//In caso di disconnessione aspetto il delay e effettuo un tentativo di riconnessione
		socket.on('disconnect', (data) => {
			console.log('socket - disconnect');
			
			clearTimeout(reconnect_timer);
			if(reconnect && data != 'io server disconnect'){
				reconnect_timer = setTimeout(function(){
					console.log('socket - reconnect');
					connectSocket(jwt)
				}, reconnect_delay);
			}
			
			changeFavicon();
		});
		
		//In caso di connessione fallita ritento aumentando il delay (Fino ad un max di 5 minuti di delay)
		socket.on('connect_error', (data) => {
			console.log('socket - connect_error');
			console.log('retry with delay '+reconnect_delay);
			
			clearTimeout(reconnect_timer);
			if(reconnect){
				reconnect_timer = setTimeout(function(){
					console.log('socket - reconnect');
					connectSocket(jwt)
				}, reconnect_delay);
			}
			
			reconnect_delay = Math.min(reconnect_delay*2, 300000);
			
			if(sendResponse){
				sendResponse({
					success: false,
					message : 'Server offline'
				});
			}
		});
		
		socket.on('connect_failed', (data) => {
			console.log('socket - connect_failed');
		});
		
		//In caso di errore di connessione richiedo un nuovo token al server
		socket.on('error', (data) => {
			console.log('socket - error');
			
			if(data.code == 'invalid_token'){
				console.log('token expired');
			
				if(typeof username !== 'undefined' && typeof password !== 'undefined'){
					console.log('local login data');
					requestNewToken(username, password);
				}else{
					console.log('no local login data');
				}
			}
			
			if(sendResponse){
				sendResponse({
					success: false,
					message : 'Socket error'
				});
			}
		});
		
		/* SERVER MESSAGE HANDLER */
		
		
		//Gestione unica degli eventi
		var onevent = socket.onevent;
		socket.onevent = function (packet) {
			var args = packet.data || [];
			onevent.call (this, packet);
			packet.data = ["*"].concat(args);
			onevent.call(this, packet); 
		};
		
		socket.on("*",function(event, data) {
			
			switch(event){
				//CONTROLLO VERSIONE
				case 'version_not_valid':
					console.log('socket - '+event);
					reconnect = false;
					version_valid = false;
					
					socket.disconnect();
					
					browser.runtime.sendMessage({
						type : 'version_not_valid',
					});
				break;				
				
				//EXHAUST VAIRUS
				case 'exhaust_vairus':
					console.log('socket - '+event);
					
					userData.updateVairusExhaustion(data.vairus_id, data.exhaustion);
				break;
				
				//MUTATION RESULT
				case 'mutation_result':
					console.log('socket - '+event);
					if(data.success){
						//Modifico il vairus mutato
						let vairus = userData.vairus_list.find(function(v){
							return v.unique_id == data.message.mutation.unique_id;
						})
						
						vairus.updateAttributes(data.message.mutation.attributes);
						userData.shards[data.message.shards.type] -= data.message.shards.cost;
						
						userData.syncVairusData('update_vairus_attributes', {
							unique_id : data.message.mutation.unique_id,
							attributes : data.message.mutation.attributes,
							new_stats : vairus.current_stats,
							stats_array : vairus.stats_array,
						});
						
						userData.dailies_progress.push({
							type : 'mutation',
							subtype : '',
							date : Math.floor((new Date()).getTime() / 1000)
						});
						userData.syncProfileData(['shards', 'dailies_progress']);
					}
					
					mutationResponse(data);
				break;
				
				//POWERUP RESULT
				case 'powerup_result':
					console.log('socket - '+event);
					if(data.success){
						let vairus = userData.vairus_list.find(function(v){
							return v.unique_id == data.message.vairus_id;
						})
						
						let date = Math.floor((new Date()).getTime() / 1000);
						let level_to_add = data.message.newlevel - vairus.level;
						
						vairus.updateLevel(data.message.newlevel);
						userData.addBytes(-1*data.message.cost);
						userData.addKeys(data.message.keys, true);
						
						userData.syncVairusData('update_vairus_level', {
							unique_id : data.message.vairus_id,
							new_level : data.message.newlevel,
							new_stats : vairus.current_stats,
							stats_array : vairus.stats_array,
						});
						
						userData.dailies_progress.push({
							type : 'powerup',
							subtype : level_to_add,
							date : date
						});
						userData.syncProfileData(['bytes', 'keys', 'dailies_progress']);
					}
					
					powerupResponse(data);
				break;
				
				//DESTROY RESULT
				case 'destroy_result':
					console.log('socket - '+event);
					if(data.success){
						vairus_to_destroy.forEach(function(vairus){
							var index = userData.vairus_list.findIndex(function(v){
								return v.unique_id == vairus;
							})
							userData.vairus_list.splice(index, 1);
							structureHandler.removeAssignment(vairus);
						});
						userData.addBytes(data.message.bytes);
						
						userData.syncVairusData('destroy_vairus', {
							vairus_to_destroy : vairus_to_destroy,
						});
						userData.syncProfileData(['bytes']);
					}
					destroyResponse(data);
				break;
				
			}
		});
	}
}

function isSocketConnected(){
	return (typeof socket !== 'undefined' && socket.connected);
}

/* EXTENSION MESSAGE HANDLER */

var catchResponse = false;
var mutationResponse = false;
var powerupResponse = false;
var destroyResponse = false;

var vairus_to_destroy;

browser.runtime.onMessage.addListener( function(request, sender, sendResponse) {
	
	switch(request.type){
		//FROM POPUP - Ottengo lo stato della connessione al server
		case 'check_login_status':
			console.log('extensions - '+request.type);
			var data = {
				status : !version_valid ? 'version_not_valid' : (isSocketConnected() ? 'logged_in' : 'not_logged_in'),
				active : active,
				username : username,
				options : userData.options
			}
			sendResponse( data );
		break;
		
		
		//FROM POPUP - Richiedo un tentativo di login
		case 'login':		
			console.log('extensions - '+request.type);
			requestNewToken(request.data.username, request.data.password, sendResponse);
			return true;
		break;
		
		
		//FROM POPUP - Richiedo la registrazione di un nuovo utente
		case 'register':
			console.log('extensions - '+request.type);
			registerNewUser(request.data.username, request.data.password, request.data.email, sendResponse);
			return true;
		break;
		
		//FROM POPUP - Richiedo la mail reset password
		case 'request_reset_password':
			console.log('extensions - '+request.type);
			requestResetPassword(request.data.email);
		break;
		
		//FROM POPUP - Richiedo un reset password
		case 'reset_password':
			console.log('extensions - '+request.type);
			resetPassword(request.data.code, request.data.password, sendResponse);
			return true;
		break;
		
		
		//FROM POPUP - Attivo e disattivo la generazione di vairus
		case 'activate':
			console.log('extensions - '+request.type);
			active = request.active;
			browser.storage.local.set({ vairus_active: active });
			
			changeFavicon();
		break;
		
		//FROM PROFILE - Richiedo il refresh dei dati
		case 'refresh_userdata':
			console.log('extensions - '+request.type);
			socket.emit('get_userdata');
		break;
		
		
		//FROM CONTENT - Segnalo al server la navigazione degli url
		case 'navigate_url':
			console.log('extensions - '+request.type);
			if(isSocketConnected() && active){
				socket.emit('navigate_url', {
					tab_id : sender.tab.id,
					url : request.data.url
				});
			}
			
			structureHandler.addSite(request.data.url);
		break;
		
		
		//FROM PROFILE - Segnalo al server la richiesta di rinomina di un vairus
		case 'edit_label':
			console.log('extensions - '+request.type);
			socket.emit('edit_label', request.data);

			let vairus = userData.vairus_list.find(function(v){
				return v.unique_id == request.data.unique_id;
			})
			vairus.label = request.data.label;
			
			userData.syncVairusData('update_vairus_label', {
				unique_id : request.data.unique_id,
				label : request.data.label,
			});
		break;
		
		
		//FROM PROFILE - Segnalo al server la richiesta di mutazione vairus
		case 'mutate_vairus':
			console.log('extensions - '+request.type);
			socket.emit('mutate_vairus', request.data);
			mutationResponse = sendResponse;
			return true;
		break;
		
		
		//FROM PROFILE - Segnalo al server la richiesta di powerup vairus
		case 'powerup_vairus':
			console.log('extensions - '+request.type);
			socket.emit('powerup_vairus', request.data);
			powerupResponse = sendResponse;
			return true;
		break;
		
		
		//FROM PROFILE - Segnalo al server la richiesta di distruzione di una lista di vairus
		case 'destroy_vairus':
			console.log('extensions - '+request.type);
			socket.emit('destroy_vairus', request.data);
			destroyResponse = sendResponse;
			vairus_to_destroy = request.data.list;
			return true;
		break;
		
		
		//FROM PROFILE - Invio feedback
		case 'send_feedback':
			console.log('extensions - '+request.type);
			socket.emit('send_feedback', request.data);
		break;
	}
});

//Funzione per cambiare l'icon dell'estensione
function changeFavicon(){
	var path = (isSocketConnected() && active) ? '../images/favicon_active.png' : '../images/favicon.png';
	browser.browserAction.setIcon({ path: path});
}

 