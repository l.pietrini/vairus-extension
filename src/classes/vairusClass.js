
export default class Vairus {
	constructor(vairus_type, unique_id, label, attributes, level, bonus_points, catch_time, exhaustion, user_id, ended){
		this.vairus_type = vairus_type;
		this.unique_id = unique_id;
		
		this.user_id = user_id;
		this.ended = ended;
		this.label = label;
		this.attributes = attributes;
		this.level = level;
		this.bonus_points = bonus_points;
		this.catch_time = catch_time;
		this.exhaustion = exhaustion;
		
		this.current_stats = this.getCurrentStats();
		this.stats_array = this.getStatsArrayValues();
		this.potential = this.getPotential();
	}
	
	//Calculate Vairus statistics
	getCurrentStats(){
		var stats = {};
		var stats_array = ['hazard', 'secrecy', 'adaptability', 'rapidity'];
		var bonus_points_string = ('0000'+this.bonus_points).slice(-4);
		
		stats_array.forEach(function(stat,index){
			var base_stat = this.vairus_type.stats[stat];
			
			var bonus_points =  parseInt(bonus_points_string[index])+1;
			var value = base_stat.min + Math.ceil((((base_stat.max-base_stat.min)+bonus_points)*this.level)/50);
			
			stats[stat] = {
				base : value,
				total : value,
				affinities : {}
			}
			
			var affinities;
			
			switch(index){
				case 0: affinities = ['fire', 'earth']; break;
				case 1: affinities = ['water', 'acid']; break;
				case 2: affinities = ['nature', 'electric']; break;
				case 3: affinities = ['air', 'ice']; break;
			}
			
			affinities.forEach(function(attribute){
				if(this.attributes.indexOf(attribute) !== -1){
					var increment = Math.floor(stats[stat].base*0.2);
					stats[stat].total += increment;
					stats[stat].affinities[attribute] = increment;
				}
			}.bind(this));
			
			if(this.attributes.indexOf('glitch') !== -1){
				var increment = Math.floor(stats[stat].base*0.2);
				stats[stat].total += increment;
				stats[stat].affinities.glitch = increment;
			}
		}.bind(this));
		return stats;
	}
	
	updateAttributes(attributes){
		this.attributes = attributes;
		this.current_stats = this.getCurrentStats();
		this.stats_array = this.getStatsArrayValues();
		this.potential = this.getPotential();
	}
	
	updateLevel(level){
		this.level = level;
		this.current_stats = this.getCurrentStats();
		this.stats_array = this.getStatsArrayValues();
		this.potential = this.getPotential();
	}
	
	getStatsArrayValues(){
		var stats_array = ['hazard', 'secrecy', 'adaptability', 'rapidity'];
		var stats_array_values = [];
		
		for(var i=0;i<stats_array.length;i++){
			stats_array_values.push(this.current_stats[stats_array[i]].total);
		}
		
		return stats_array_values;
	}
	
	getPotential(){
		let bonus_points_string = ('0000'+this.bonus_points).slice(-4);
		let total = 0;
		
		for(let i=0; i<4; i++){
			total+= parseInt(bonus_points_string[i])+1;
		}
		
		return Math.floor((total*100)/40);
	}
}