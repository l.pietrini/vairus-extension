import raritiesData from '../data/rarities.json';

class Rarity {
	constructor(id, name, multiplier, bytes){
		this.id = id;
		this.name = name;
		this.multiplier = multiplier;
		this.bytes = bytes;
	}
}

class RaritiesManager {
	constructor(){
		this.list = [];
		
		raritiesData.forEach(function(rarity){
			this.list.push(rarity);
		}.bind(this));
	}
	
	addRarity(rarity){
		this.list.push(rarity);
	}
	
	getRarityById(id){
		var rarity = this.list.find(function(x){ return x.id == id });
		return (typeof rarity !== 'undefined')? rarity : false;
	}
	
	getRarityByName(name){
		var rarity = this.list.find(function(x){ return x.name == name });
		return (typeof rarity !== 'undefined')? rarity : false;
	}
}

export default new RaritiesManager();