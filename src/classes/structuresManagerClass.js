import structureData from '../data/structures.json';

class StructuresManager {
	constructor(){
		this.structures = {};
		
		structureData.forEach(function(structure){
			this.structures[structure.id] = structure;
		}.bind(this));
	}
	
	getLevelupCost(structure_id, to_level){
		var structure = this.structures[structure_id];
		var cost = structure.base_cost;
		
		if(structure.cost_increment_property == 'multiplicative'){
			cost = Math.floor(structure.base_cost * Math.pow(structure.cost_increment, to_level-1));
		}else{
			cost = structure.base_cost + (structure.cost_increment*(to_level-1));
		}
		
		return cost;
	}
	
	getEffect(structure_id, level){
		var structure = this.structures[structure_id];
		var effect = structure.base_effect;
		
		if(structure.effect_increment_property == 'multiplicative'){
			effect = structure.base_effect * Math.pow(structure.effect_increment, (level-1));
		}else{
			effect = structure.base_effect + (structure.effect_increment*(level-1));
		}
		
		return effect;
	}
	
	getStat(structure_id){
		var structure = this.structures[structure_id];
		return structure.stat;
	}
}

export default new StructuresManager();