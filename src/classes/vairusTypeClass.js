
export default class VairusType {
	constructor(id, name, label, biome, rarity, affinities, stats){
		this.id = id;
		this.name = name;
		this.label = label;
		this.biome = biome;
		this.rarity = rarity;
		this.affinities = affinities;
		this.stats = stats;
	}
}