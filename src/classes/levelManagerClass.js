import raritiesManager from '../classes/raritiesManagerClass.js';

class LevelManager {
	constructor(){
		this.levels = {
			1 :  { cost: 5,   cumulative: 5 },
			2 :  { cost: 10,  cumulative: 15 },
			3 :  { cost: 15,  cumulative: 30 },
			4 :  { cost: 20,  cumulative: 50 },
			5 :  { cost: 25,  cumulative: 75 },
			6 :  { cost: 30,  cumulative: 105 },
			7 :  { cost: 35,  cumulative: 140 },
			8 :  { cost: 40,  cumulative: 180 },
			9 :  { cost: 45,  cumulative: 225 },
			10 : { cost: 50,  cumulative: 275 },
			11 : { cost: 60,  cumulative: 335 },
			12 : { cost: 70,  cumulative: 405 },
			13 : { cost: 80,  cumulative: 485 },
			14 : { cost: 90,  cumulative: 575 },
			15 : { cost: 100, cumulative: 675 },
			16 : { cost: 110, cumulative: 785 },
			17 : { cost: 120, cumulative: 905 },
			18 : { cost: 130, cumulative: 1035 },
			19 : { cost: 140, cumulative: 1175 },
			20 : { cost: 150, cumulative: 1325 },
			21 : { cost: 165, cumulative: 1490 },
			22 : { cost: 180, cumulative: 1670 },
			23 : { cost: 195, cumulative: 1865 },
			24 : { cost: 210, cumulative: 2075 },
			25 : { cost: 225, cumulative: 2300 },
			26 : { cost: 240, cumulative: 2540 },
			27 : { cost: 255, cumulative: 2795 },
			28 : { cost: 270, cumulative: 3065 },
			29 : { cost: 285, cumulative: 3350 },
			30 : { cost: 300, cumulative: 3650 },
			31 : { cost: 320, cumulative: 3970 },
			32 : { cost: 340, cumulative: 4310 },
			33 : { cost: 360, cumulative: 4670 },
			34 : { cost: 380, cumulative: 5050 },
			35 : { cost: 400, cumulative: 5450 },
			36 : { cost: 420, cumulative: 5870 },
			37 : { cost: 440, cumulative: 6310 },
			38 : { cost: 460, cumulative: 6770 },
			39 : { cost: 480, cumulative: 7250 },
			40 : { cost: 500, cumulative: 7750 },
			41 : { cost: 525, cumulative: 8275 },
			42 : { cost: 550, cumulative: 8825 },
			43 : { cost: 575, cumulative: 9400 },
			44 : { cost: 600, cumulative: 10000 },
			45 : { cost: 625, cumulative: 10625 },
			46 : { cost: 650, cumulative: 11275 },
			47 : { cost: 675, cumulative: 11950 },
			48 : { cost: 700, cumulative: 12650 },
			49 : { cost: 725, cumulative: 13375 },
			50 : { cost: 750, cumulative: 14125 }
		}
	}
	
	getRarityMultiplier(rarity){
		var rarity = raritiesManager.getRarityByName(rarity);
		return rarity.id;
	}
	
	getCost(level, rarity){
		return this.levels[level].cost * this.getRarityMultiplier(rarity);
	}
	
	getCumulative(level, rarity){
		return this.levels[level].cumulative * this.getRarityMultiplier(rarity);
	}
	
	getCostRange(start_level, end_level, rarity){
		return this.getCumulative(end_level, rarity) - this.getCumulative(start_level, rarity);
	}
	
	getKeysRange(start_level, end_level){
		return [
			(start_level < 10 && end_level >= 10)? 1 : 0,
			(start_level < 20 && end_level >= 20)? 1 : 0,
			(start_level < 30 && end_level >= 30)? 1 : 0,
			(start_level < 40 && end_level >= 40)? 1 : 0,
		];
	}
	
	getPhantomStats(vairus, level){
		var stats = {};
		var stats_array = ['hazard', 'secrecy', 'adaptability', 'rapidity'];
		var bonus_points_string = ('0000'+vairus.bonus_points).slice(-4);
		
		stats_array.forEach(function(stat,index){
			var base_stat = vairus.vairus_type.stats[stat];
			
			var bonus_points =  parseInt(bonus_points_string[index])+1;
			var value = base_stat.min + Math.ceil((((base_stat.max-base_stat.min)+bonus_points)*level)/50);
			
			stats[stat] = {
				base : value,
				total : value,
				affinities : {}
			}
			
			var affinities;
			
			switch(index){
				case 0: affinities = ['fire', 'earth']; break;
				case 1: affinities = ['water', 'acid']; break;
				case 2: affinities = ['nature', 'electric']; break;
				case 3: affinities = ['air', 'ice']; break;
			}
			
			affinities.forEach(function(attribute){
				if(vairus.attributes.indexOf(attribute) !== -1){
					var increment = Math.floor(stats[stat].base*0.2);
					stats[stat].total += increment;
					stats[stat].affinities[attribute] = increment
				}
			}.bind(this));
		}.bind(this));
		
		return stats;
	}
}

export default new LevelManager();