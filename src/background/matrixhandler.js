var browser = browser || chrome;

import structuresManager from '../classes/structuresManagerClass.js';
import Vairus from '../classes/vairusClass.js';
import crypto from 'crypto';

export default class MatrixHandler {
	constructor(userData, structureHandler){
		this.userData = userData;
		this.structureHandler = structureHandler;
		this.socket = null;
		this.matrixAssignResponse = null;
		this.matrix_data = [];
		
		/* EXTENSION EVENTS */
		
		chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			
			switch(request.type){
				//FROM PROFILE - Request for the profile data
				case 'request_userdata':
					console.log('extensions - userdata - '+request.type);
					this.syncMatrixContent();
					return false;
					
				//FROM CONTENT - Signal for an assignment to a matrix
				case 'matrix_assign_vairus':
					console.log('extensions - matrix - '+request.type);
					var url = new URL(request.data.url);
					var md5 = crypto.createHash('md5');
					md5.update(url.host);
					var digest = md5.digest('hex');
					
					if(this.isSocketConnected()){
						this.socket.emit('matrix_assign_vairus', {
							vairus_id : request.data.vairus_id,
							url : digest
						});
					}
					this.matrixAssignResponse = sendResponse;
					return true;
			}
			
		}.bind(this));
	}
	
	updateSocket(socket){
		this.socket = socket;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			switch(event){
				//Spawn matrix message
				case 'spawn_matrix':
					console.log('socket - matrix - '+event);
						
					if( this.userData.checkTutorial('network')){
						let score_boost = 1;
						
						var node = this.structureHandler.nodes_data.find(function(n){
							return n.hash == data.host;
						});
						
						if(node != undefined){
							let structure = node.structures.find(function(structure){
								return structure.id == 'firewall';
							});
							score_boost = structuresManager.getEffect(structure.id, structure.level);
						}
						
						chrome.tabs.sendMessage(data.tab_id, {
							type : 'spawn_matrix',
							data : { 
								level : data.level,
								expiration : data.expiration,
								main_stat : data.main_stat,
								secondary_stat : data.secondary_stat,
								current_score : data.current_score,
								current_vairus : data.current_vairus,
								current_users : data.current_users,
								progress : data.progress,
								score_boost : score_boost,
								energy : data.energy,
								vairus : this.userData.vairus_list.find(function(v){ return v.unique_id == data.vairus; })
							}
						});
					}
				break;
				
				//Start matrix
				case 'matrix_assign_vairus':
					console.log('socket - matrix - '+event);
					
					if(data.success){
						this.userData.updateVairusExhaustion(data.message.vairus_id, 'matrix');
						
						//Daily progress
						this.userData.dailies_progress.push({
							type : 'matrix',
							subtype : '',
							date : Math.floor((new Date()).getTime() / 1000)
						});
						
						this.userData.syncProfileData(['dailies_progress']);
					}
					
					this.matrixAssignResponse(data);
				break;
				
				//Matrix rewards
				case 'matrix_reward':
					console.log('socket - matrix - '+event);
					
					this.userData.addBytes(data.bytes);
					this.userData.addShards(data.shards);
					
					let keys = [0,0,0,0];
					for(let type in data.keys){
						keys[type-1]+=data.keys[type];
					}
					this.userData.addKeys(keys, false);
					this.userData.addVairusProgress(data.v_progress, data.level-1);
					this.userData.syncProfileData(['bytes', 'shards', 'keys', 'v_progress']);
					
					if(data.vairus != null){
						var v = data.vairus;
						
						var vairus = new Vairus(
							this.userData.dex[v.vairus_id], 
							v.unique_id,
							v.label, 
							v.attributes, 
							v.level, 
							v.bonus_points,
							v.catch_time,
							v.exhaustion,
							0,
							0
						);
						this.userData.addVairus(vairus, false);
						data.vairus = v.vairus_id;
					}
					
					data.hkeys = data.keys;
					data.time = Math.floor(new Date().getTime()/1000);
					
					//Exhausting the vairus
					this.userData.vairus_list.forEach(function(vairus){
						if(vairus.exhaustion == 'matrix'){
							this.userData.updateVairusExhaustion(vairus.unique_id, new Date().getTime()+28800000);
						}
					}.bind(this));
					
					
					this.matrix_data.unshift(data);
					this.syncMatrixContent();
				break;
				
				//Request rewards
				case 'matrixdata':
					console.log('socket - matrix - '+event);
					this.matrix_data = data.map(function(reward){
						reward.hkeys = JSON.parse(reward.hkeys);
						reward.shards = JSON.parse(reward.shards);
						return reward;
					});
					this.syncMatrixContent();
				break;
			}
		}.bind(this));
	}
	
	syncMatrixContent(){
		chrome.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == chrome.runtime.getURL('/profile/profile.html')){
					chrome.tabs.sendMessage(tab.id, {
						type : 'sync_matrixes',
						data : this.matrix_data
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}
}