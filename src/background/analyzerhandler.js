import crypto from 'crypto';
import Vairus from '../classes/vairusClass.js';

var browser = browser || chrome;

export default class AnalyzerHandler {
	constructor(userData){
		this.userData = userData;
		this.socket = null;
		this.milestones = 0;
		this.info_fragments = {};
		this.fragments_changes = [];
		this.analyzer = null;
		this.analysis = {};
		this.probes_rewards = [];
		this.analysis_count = 0;
		this.requested_analysis = {};
		
		browser.storage.local.get(['analysis'], function(result) {
			if(result.hasOwnProperty('analysis')) this.analysis = result.analysis;
		}.bind(this));
				
		setInterval(function(){
			let date = new Date();
			let day = date.getUTCFullYear() +'-'+ date.getUTCMonth() +'-'+ date.getUTCDate();
			let current_timestamp = Math.floor(Date.now()/1000);
			
			//Removing old analysis
			for(var host in this.analysis){
				if(day != this.analysis[host].day){
					delete this.analysis[host];
					delete this.requested_analysis[host];
				}
			}
			
			//Removing old probe rewards
			this.probes_rewards = this.probes_rewards.filter(function(r){
				return r.time > current_timestamp-(48*60*60);
			});
			
			//Resetting the analysis index
			if(Object.keys(this.requested_analysis).length > 1000){
				this.requested_analysis = {};
			}
			
			browser.storage.local.set({ analysis: this.analysis });
			
			this.syncAnalyzerData();
		}.bind(this), 10*60*1000);
		
		
		/* EXTENSION EVENTS */
		
		chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			switch(request.type){
				//FROM PROFILE
				case 'request_userdata':
					this.syncAnalyzerData();
					return false;
					
				//FROM PROFILE
				case 'remove_change':
					var index = this.fragments_changes.findIndex(function(c){
						return c.set == request.data.set && c.info == request.data.info;
					});
					this.fragments_changes.splice(index, 1);
					
					this.syncAnalyzerData();
					return false;
					
				case 'upgrade_analyzer':
					this.socket.emit(request.type, request.index);
					return false;
					
				//FROM CONTENT 
				case 'analyze_website':					
				case 'get_info_fragment':					
				case 'send_probe':					
					var url = new URL(request.data.url);
					var md5 = crypto.createHash('md5');
					md5.update(url.host);
					var digest = md5.digest('hex');
					
					if(request.type == 'analyze_website'){
						this.requested_analysis[digest] = url.host;
					}
					
					if(this.isSocketConnected()){
						this.socket.emit(request.type, {
							tab_id : sender.tab.id,
							url : digest
						});
					}
				break;
			}
		}.bind(this));		
	}
	
	updateSocket(socket){
		this.socket = socket;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			switch(event){
				case 'analyzerdata':
					this.milestones = data.milestones;
					this.info_fragments = data.info_fragments;
					this.analyzer = data.analyzer;
					this.probes_rewards = data.probes_rewards;
					
					this.syncAnalyzerData();
				break;
				
				case 'analysis_result':
					this.analysis_count++;
					this.analysis[data.host] = data.analysis;
					this.analysis[data.host].day = data.day;
					this.analysis[data.host].url = this.requested_analysis[data.host];
					delete this.requested_analysis[data.host];
				
					chrome.tabs.sendMessage(data.tab_id, {
						type : 'analysis_result',
						data : data.analysis,
					});
					
					this.analyzer.energy = data.new_energy;
					this.analyzer.last_analysis = data.time;
					
					browser.storage.local.set({ analysis: this.analysis });
			
					this.syncAnalyzerData();
				break;
				
				case 'get_info_result':
					this.milestones++;
						
					if(data.rewards.vairus){
						var v = data.rewards.vairus;
						var vairus = new Vairus(
							this.userData.dex[v.vairus_id], 
							v.unique_id,
							v.label, 
							v.attributes, 
							v.level, 
							v.bonus_points,
							v.catch_time,
							v.exhaustion,
							0,
							0
						);
						this.userData.addVairus(vairus, false);
					
						data.rewards.vairus = vairus;
					}
					
					if(data.rewards.hasOwnProperty('info')){
						var current_set = this.info_fragments[data.rewards.info.set];
						for(var i=0; i<current_set.length; i++){
							if(current_set[i] == '0' && data.rewards.info.value[i] == '1'){
								var info = Math.floor(i/5);
								
								this.fragments_changes.push({
									set : data.rewards.info.set,
									info : info
								})
								break;
							}
						}
						
						this.info_fragments[data.rewards.info.set] = data.rewards.info.value;
					}
					
					this.userData.addBytes(data.rewards.bytes);
					this.userData.syncProfileData(['bytes']);
					
					this.analysis[data.host].info = false;
					
					browser.storage.local.set({ analysis: this.analysis });
			
					this.syncAnalyzerData();
					
					chrome.tabs.sendMessage(data.tab_id, {
						type : 'get_info_result',
						result_type : data.type,
						data : data.rewards
					});
				break;
				
				case 'send_probe_result':
					if(data.success){
						this.userData.addProcess(data.message);
						this.userData.syncProfileData(['processes']);
					}
				break;
				
				case 'probe_rewards':
					this.userData.removeProcess(data.process.id);
					this.userData.syncProfileData(['processes']);
					
					this.probes_rewards.push({
						data : data.rewards,
						time : Math.floor(Date.now()/1000),
					});
					
					if(this.analysis.hasOwnProperty(data.host)){
						this.analysis[data.host].probed = true;
					}
					
					browser.storage.local.set({ analysis: this.analysis });
			
					this.syncAnalyzerData();
					
					var rewards = JSON.parse(data.rewards);
					this.userData.addBytes(rewards.bytes);
					this.userData.addShards(rewards.shards);
					this.userData.addChips(rewards.chips);
					this.userData.addKeys(rewards.keys, false);
					
					this.userData.syncProfileData(['bytes', 'shards', 'keys', 'chips']);
					
					this.sendActiveTabNotification('probe_rewards_notification', 'azure');
				break;
				
				case 'upgrade_analyzer_result':
					if(data.success){
						var upgrade = 'u'+(parseInt(data.message.index)+1);

						this.analyzer[upgrade]++;
						this.userData.addChips(-1 * data.message.cost);
						this.userData.syncProfileData(['chips']);

						if(upgrade == 'u1'){
							this.analyzer.max_energy+=10;
						}

						if(upgrade == 'u2'){
							this.analyzer.energy_regen+=1;
						}
					}
					this.syncAnalyzerActions('analyzer_update', this.analyzer);
				break;
				break;
			}
		}.bind(this));
	}
	
	analysisRequest(info, tab){
		browser.tabs.sendMessage(tab.id, {
			type : 'analysis_request',
			data : this.analyzer
		});
	}
	
	syncAnalyzerData(){
		chrome.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == chrome.runtime.getURL('/profile/profile.html')){
					chrome.tabs.sendMessage(tab.id, {
						type : 'sync_data',
						data : {
							milestones : this.milestones,
							info_fragments : this.info_fragments,
							fragments_changes : this.fragments_changes,
							analyzer : this.analyzer,
							probes_rewards : this.probes_rewards,
							analysis_count : this.analysis_count,
							analysis : this.analysis
						}
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	//Syncronization of the network data with the profile pages
	syncAnalyzerActions(type, data){
		browser.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == browser.runtime.getURL('/profile/profile.html')){
					browser.tabs.sendMessage(tab.id, {
						type : 'sync_analyzer',
						data : {
							type : type,
							data : data
						}
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	//Send a notification to the active tab
	sendActiveTabNotification(message, color = 'azure'){
		browser.tabs.query({active: true, currentWindow: true}, function(tabs){
			tabs.forEach(function(tab){
				browser.tabs.sendMessage(tab.id, {
					type : 'generic_notification',
					data : {
						message : message,
						color : color
					}
				});
			}.bind(this));
		});
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}
}