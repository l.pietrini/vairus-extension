var browser = browser || chrome;

import vairusData from '../data/vairus.json';
import Vairus from '../classes/vairusClass.js';
import VairusType from '../classes/vairusTypeClass.js';

export default class UserData {
	constructor(){
		let date = new Date();
		
		this.dex = {};
		this.vairus_list = [];
		this.processes = [];
		this.options = {};
		this.links = {};
		this.dailies_seed = date.getUTCDate()+'-'+(date.getUTCMonth()+1)+'-'+date.getUTCFullYear();
		this.dailies_progress = [];
		this.dailies_rewards = [];
		this.boxes = [
			{name:'Box 1',vairus: []},
			{name:'Box 2',vairus: []},
			{name:'Box 3',vairus: []},
			{name:'Box 4',vairus: []},
			{name:'Box 5',vairus: []}
		];
		this.tutorial = [
			{ flag : 'start', value : 1 },
			{ flag : 'first_catch', value :  1 },
			{ flag : 'first_objective', value :  1 },
			{ flag : 'network', value :  1 },
			{ flag : 'network_profile', value :  1 },
			{ flag : 'matrix', value :  1 },
			{ flag : 'matrix_details', value :  1 },
			{ flag : 'introduction', value :  1 },
			{ flag : 'analyzer', value :  1 },
			{ flag : 'analyzer_profile', value :  1 },
			{ flag : 'probe', value :  1 },
			{ flag : 'scanner', value :  1 },
		];
		this.socket = null;
		this.username = '';
		this.has_data = false;
		
		//Daily seed refresh
		setInterval(function(){
			let date = new Date();
			let new_seed = date.getUTCDate()+'-'+(date.getUTCMonth()+1)+'-'+date.getUTCFullYear();
			
			if(new_seed != this.dailies_seed){
				this.dailies_seed = new_seed;
				this.dailies_progress = [];
				this.dailies_rewards = [];
				this.syncProfileData(['dailies_seed', 'dailies_progress', 'dailies_rewards']);
			}
		}.bind(this), 10000);
		
		//Dex load
		vairusData.forEach(function(v){
			this.dex[v.id] = new VairusType(v.id, v.name, v.label, v.biome, v.rarity, v.affinities, v.stats);
		}.bind(this));
		
		/* STORAGE */
		
		browser.storage.local.get(['vairus_boxes', 'options'], function(result) {
			if(result.hasOwnProperty('vairus_boxes')) this.boxes = JSON.parse(result.vairus_boxes);
			if(result.hasOwnProperty('options')) this.options = JSON.parse(result.options);
		}.bind(this));
		
		
		/* EXTENSION EVENTS */
		
		browser.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			
			switch(request.type){
				//FROM PROFILE - Request profile data
				case 'request_userdata':
					console.log('extensions - userdata - '+request.type);
					this.syncVairusData('all_vairus', this.vairus_list);
					this.syncProfileData(['username', 'bytes', 'chips', 'shards', 'keys', 'v_progress', 'tutorial', 'boxes', 'processes', 'options', 'links', 'dailies_seed', 'dailies_progress', 'dailies_rewards']);
					return false;
				
				//FROM EVERYWHERE - Stop a process
				case 'stop_process':
					console.log('extensions - '+request.type);
					this.socket.emit('stop_process', request.data);
				break;
				
				
				//FROM PROFILE - Update the boxes (Local storage only)
				case 'update_boxes':
					console.log('extensions - userdata - '+request.type);
					this.boxes = request.data;
					browser.storage.local.set({ vairus_boxes: JSON.stringify(request.data) });
					this.syncProfileData(['boxes']);
				break;
				
				
				//FROM PROFILE - Update box label
				case 'edit_box_label':
					console.log('extensions - userdata - '+request.type);
					this.boxes[request.data.index-1].name = request.data.label;
					browser.storage.local.set({ vairus_boxes: this.boxes });
					this.syncProfileData(['boxes']);
				break;
				
				
				//FROM EVERYWHERE - Request the list of Vairus
				case 'request_list':
					console.log('extensions - userdata - '+request.type);
					sendResponse({
						vairus_list : this.vairus_list,
						boxes : this.boxes
					});
					return true;
				
				
				//FROM EVERYWHERE - Set a tutorial flag
				case 'tutorial_flag':
					console.log('extensions - userdata - '+request.type);
					let flag = this.tutorial.find(function(f){
						return f.flag == request.data;
					});
					
					if(typeof flag !== 'undefined' && flag.value == 0){
						flag.value = 1;
						
						this.syncProfileData(['tutorial']);
						this.socket.emit('tutorial_flag', { flag : request.data });
					}
				break;
				
				
				//FROM PROFILE - Change options
				case 'set_option':
					console.log('extensions - '+request.type);
					this.options[request.data.option] = request.data.value;
					browser.storage.local.set({ options: JSON.stringify(this.options) });
					this.syncProfileData(['options']);
				break;
				
				
				//FROM PROFILE - Complete a daily mission
				case 'complete_daily':
					console.log('extensions - '+request.type);
					this.socket.emit('complete_daily', request.data);
				break;
			}
		}.bind(this));
	}
	
	updateSocket(socket, username){
		this.socket = socket;
		this.username = username;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			
			switch(event){
				case 'userdata':
					console.log('socket - userdata - '+event);
					var userdata = JSON.parse(window.atob(data));
					
					this.vairus_list = [];
					userdata.vairus_list.forEach(function(v){
						this.vairus_list.push(new Vairus(
							this.dex[v.vairus_id], 
							v.unique_id,
							v.label, 
							v.attributes, 
							v.level, 
							v.bonus_points,
							v.catch_time,
							v.exhaustion,
							0,
							0
						));
					}.bind(this));
					
					this.has_data = true;
					this.bytes = userdata.bytes;
					this.chips = userdata.chips;
					this.shards = userdata.shards;
					this.keys = userdata.keys;
					this.v_progress = userdata.v_progress;
					this.processes = userdata.processes;
					this.dailies_progress = userdata.dailies_progress;
					this.dailies_rewards = userdata.dailies_rewards;
					this.links = userdata.links;
					
					for(let i = 0; i < userdata.tutorial.length; i++) {
						if(i < this.tutorial.length){
							this.tutorial[i].value = parseInt(userdata.tutorial[i]);
						}
					}
					
					this.syncVairusData('all_vairus', this.vairus_list);
					this.syncProfileData(['username', 'bytes', 'chips', 'shards', 'keys', 'v_progress', 'tutorial', 'boxes', 'processes', 'options', 'links', 'dailies_seed', 'dailies_progress', 'dailies_rewards']);
				break;
				
				case 'stopped_process':
					console.log('socket - '+event);
					this.removeProcess(data);
					this.syncProfileData(['processes']);
				break;	
				
				case 'complete_daily':
					console.log('socket - '+event);
					this.dailies_rewards.push({
						daily : data.index,
						date : Math.floor((new Date()).getTime() / 1000)
					});
					this.addBytes(data.reward);
					this.syncProfileData(['bytes', 'dailies_rewards']);
				break;
			}
		}.bind(this));
	}
	
	//Exhaust a Vairus
	updateVairusExhaustion(unique_id, exhaustion){
		var index = this.vairus_list.findIndex(function(vairus){
			return vairus.unique_id == unique_id;
		});
		this.vairus_list[index].exhaustion = exhaustion;
		
		this.syncVairusData('update_vairus_exhaustion', {
			unique_id : unique_id,
			exhaustion : exhaustion
		});
	}
	
	//Add a Vairus to the list
	addVairus(vairus, update_daily = false){
		this.vairus_list.push(vairus);
		this.syncVairusData('add_vairus', vairus);
		this.syncProfileData(['dailies_progress']);
		
		if(update_daily){
			this.dailies_progress.push({
				type : 'vairus',
				subtype : vairus.vairus_type.biome,
				date : Math.floor((new Date()).getTime() / 1000)
			});
		}
	}
	
	//Add bytes
	addBytes(value){
		this.bytes+=value;
	}
	
	//Add chips
	addChips(value){
		this.chips+=value;
	}

	//Add shards
	addShards(new_shards){
		for(var type in new_shards){
			this.shards[type] += new_shards[type];
		}
	}
	
	//Add keys
	addKeys(keys, negative){
		for(var i=0; i<this.keys.length; i++){
			if(negative) this.keys[i] -= keys[i];
			else		 this.keys[i] += keys[i];
		}
	}
	
	//Add Vairu progress for the matrix
	addVairusProgress(progress, type){
		this.v_progress[type] = progress;
	}
	
	//Add a process
	addProcess(process){
		this.processes.push(process);
	}
	
	//Remove a process
	removeProcess(process_id){
		let index = this.processes.findIndex(function(p){
			return p.id == process_id;
		});
		
		if(index !== -1){
			this.processes.splice(index, 1);
		}
	}
	
	//Check if a tutorial is complete
	checkTutorial(flag){
		let f = this.tutorial.find(function(f){
			return f.flag == flag;
		});
		
		if(typeof f === 'undefined') return false;
		else return f.value == 1;
	}
	
	//Syncronize Vairus data for the profile pages
	syncVairusData(type, data){
		browser.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == browser.runtime.getURL('/profile/profile.html')){
					browser.tabs.sendMessage(tab.id, {
						type : 'sync_vairus',
						data : {
							type : type,
							data : data
						}
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	//Syncronize specific data for the profile pages
	syncProfileData(types){
		browser.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == browser.runtime.getURL('/profile/profile.html')){
					
					let data = {};
					types.forEach(function(type){
						data[type] = this[type];
					}.bind(this));
					
					browser.tabs.sendMessage(tab.id, {
						type : 'sync_data',
						data : data
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}

}