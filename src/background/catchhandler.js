
import Vairus from '../classes/vairusClass.js';

export default class CatchHandler {
	constructor(userData){
		this.userData = userData;
		this.socket = null;
		
		/* EXTENSION EVENTS */
		
		chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			
			switch(request.type){
				//FROM CONTENT - i signal the server about a clicked spawn
				case 'spawn_click':
					console.log('extensions - catch - '+request.type);
					if(this.isSocketConnected()){
						this.socket.emit('spawn_click', {
							tab_id : sender.tab.id,
							unique_id : request.data.unique_id
						});
					}
				break;
			}
		}.bind(this));
		
		
	}
	
	updateSocket(socket){
		this.socket = socket;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			
			switch(event){
				//SPAWN
				case 'spawn':
					console.log('socket - spawn - '+event);
					
					chrome.tabs.sendMessage(data.tab_id, {
						type : 'spawn',
						data : { 
							type : data.type,
							unique_id : data.unique_id,
							biome : data.biome,
							list_length : this.userData.vairus_list.length
						}
					});
				break;
				
				//CLICK SPAWN RESULTS
				case 'click_spawn_result':
					console.log('socket - catch - '+event);
					
					//State update
					if(data.success){
						switch(data.message.type){
							case 'vairus':
								var v = data.message.vairus;
								var vairus = new Vairus(
									this.userData.dex[v.vairus_id], 
									v.unique_id,
									v.label, 
									v.attributes, 
									v.level, 
									v.bonus_points,
									v.catch_time,
									v.exhaustion,
									0,
									0
								);
								
								data.message.vairus = vairus;
								
								this.userData.addVairus(vairus, true);
								this.userData.addBytes(data.message.bytes);
								this.userData.addShards(data.message.shards);
								
								this.userData.syncProfileData(['bytes', 'shards']);
							break;
							case 'treasure':
								if(data.message.treasure_type == 'bytes'){
									this.userData.addBytes(data.message.bytes);
									this.userData.syncProfileData(['bytes']);
								}
								
								if(data.message.treasure_type == 'shards'){
									this.userData.addShards(data.message.shards);
									this.userData.syncProfileData(['shards']);
								}
							break;
						}
					} 
					
					data.message.list_length = this.userData.vairus_list.length;
					
					//Send message to the content script
					chrome.tabs.sendMessage(data.message.tab_id, {
						type : 'click_spawn_result',
						success : data.success,
						data : data.message
					});
				break;
				
				//SPAWN LIMIT
				case 'spawn_limit':
					console.log('socket - spawn - '+event);
					
					chrome.tabs.sendMessage(data.tab_id, {
						type : 'spawn_limit',
						limit : data.type
					});
				break;
			}
		}.bind(this));
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}
}