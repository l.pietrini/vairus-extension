var browser = browser || chrome;

import structuresManager from '../classes/structuresManagerClass.js';
import crypto from 'crypto';
	
export default class StructureHandler {
	constructor(userData){
		this.userData = userData;
		this.socket = null;
		this.last_sites = [];
		this.nodes_data = [];
		
		/* EXTENSION EVENTS */
		
		chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
			
			switch(request.type){
				case 'request_structures':
					console.log('extensions - structures - '+request.type);
					this.syncNodesData('all_data', {
						nodes : this.nodes_data,
						last_sites : this.last_sites,
					});
				break;
				case 'unlock_node':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('unlock_node', request.data);
				break;
				case 'change_host':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('change_host', request.data);
				break;
				case 'upgrade_node':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('upgrade_node', request.data);
				break;
				case 'assign_vairus':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('assign_vairus', request.data);
				break;
				case 'start_builder':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('start_builder', request.data);
				break;
				case 'start_node_process':
					console.log('extensions - structures - '+request.type);
					this.socket.emit('start_node_process', request.data);
				break;
			}
			
		}.bind(this));
	}
	
	updateSocket(socket){
		this.socket = socket;
		
		/* SERVER EVENTS*/
		socket.on("*", function(event, data) {
			switch(event){
				//Network data
				case 'structuredata':
					console.log('socket - structure - '+event);
					this.nodes_data = data;
					this.syncNodesData('all_data', {
						nodes : this.nodes_data,
						last_sites : this.last_sites,
					});
				break;
				
				//CATCH RESULTS
				case 'click_spawn_result':
					if(data.success){
						var node = this.nodes_data.find(function(n){
							return n.hash == data.message.host;
						});
						
						if(node != undefined){
							node.last_catch = new Date().getTime();
							
							this.syncNodesData('node_last_catch', {
								node_id : node.id,
								last_catch : node.last_catch
							});
						}
					}
				break;
				
				//Unlock node
				case 'unlock_node_result':
					console.log('socket - structure - '+event);
					if(data.success){
						let structures = [];
						let last_catch = null;
						
						for(let type in structuresManager.structures){
							if(structuresManager.structures[type].type != 'main'){
								structures.push({ id: type,  level : 1 });
							}
						}
						
						if(data.message.host != ''){
							let site = this.last_sites.find(function(site){ return site.url == data.message.host; });
							last_catch = site.timestamp;
						}
						
						var hash = crypto.createHash('md5');
						hash.update(data.message.host);
						var digest = hash.digest('hex')
						
						var node = {
							id : 'n'+data.message.id,
							level : 1,
							host : data.message.host,
							hash : digest,
							last_catch : last_catch,
							last_change : 0,
							structures : structures,
							assignment : null,
							progress : null
						};
						
						this.nodes_data.push(node);
						this.userData.addBytes(-1*data.message.cost);
						this.userData.syncProfileData(['bytes']);
						this.syncNodesData('unlock_node', node);
					}
				break;
				
				//Change host
				case 'change_host_result':
					console.log('socket - structure - '+event);
					
					var node = this.nodes_data.find(function(n){
						return n.id == 'n'+data.message.node_id;
					});
					
					if(node != undefined){
						node.last_change = new Date().getTime();
						node.last_catch = data.message.last_catch;
						node.host = data.message.host;
						
						this.syncNodesData('node_change_host', {
							node_id : node.id,
							last_change : node.last_change,
							last_catch : node.last_catch,
							host : node.host
						});

					}
				break;
				
				//Upgrade node
				case 'upgrade_node_result':
					console.log('socket - structure - '+event);
					if(data.success){
						var node = this.nodes_data.find(function(node){ return node.id == 'n'+data.message.node_id; });
						node.level++;
						
						this.userData.addBytes(-1 * data.message.cost);
						this.userData.syncProfileData(['bytes']);
						
						this.syncNodesData('upgrade_node', {
							node_id : node.id,
							level : node.level
						});
					}
				break;
				
				//Vairus assignment to a node
				case 'assign_vairus_result':
					console.log('socket - structure - '+event);
					if(data.success){
						var node = this.nodes_data.find(function(node){ return node.id == 'n'+data.message.node_id; });
						
						if(node.assignment != null){
							this.userData.updateVairusExhaustion(node.assignment, null);
						}
						node.assignment = data.message.vairus_id;
						
						this.userData.updateVairusExhaustion(data.message.vairus_id, 'assigned');
						this.syncNodesData('assign_vairus', {
							node_id : node.id,
							assignment : node.assignment,
						});
					}
				break;
				
				//Structure upgrade start
				case 'start_builder_result':
				case 'start_incubator_result':
				case 'start_decrypter_result':
				case 'start_compiler_result':
				case 'start_keygen_result':
					console.log('socket - structure - '+event);
					if(data.success){
						this.userData.addProcess(data.message);
						this.userData.syncProfileData(['processes']);
					}
				break;
				
				//Structure upgrade end
				case 'end_upgrade_structure':
					console.log('socket - structure - '+event);
					var node = this.nodes_data.find(function(node){ return node.id == 'n'+data.data1; });
					let structure = node.structures.find(function(structure){ return structure.id == data.data2; });
					structure.level++;
					
					this.syncNodesData('upgrade_structure', {
						node_id : node.id,
						structure_id : structure.id,
						level : structure.level
					});
					
					this.sendActiveTabNotification('structure_upgrade_finished', 'pink');
					
					this.userData.removeProcess(data.id);
					this.userData.syncProfileData(['processes']);
				break;
				
				//Incubator upgrade end
				case 'end_incubator':
					console.log('socket - structure - '+event);
					
					let vairus = this.userData.vairus_list.find(function(v){
						return v.unique_id == data.vairus_id;
					});
					
					vairus.updateLevel(vairus.level+1);
					
					this.userData.syncVairusData('update_vairus_level', {
						unique_id : data.vairus_id,
						new_level : vairus.level,
						new_stats : vairus.current_stats,
						stats_array : vairus.stats_array,
					});
					
					this.userData.removeProcess(data.id);
					this.userData.syncProfileData(['processes']);
				break;
				
				//Compiler upgrade end
				case 'end_compiler':
					console.log('socket - structure - '+event);
					
					this.userData.addBytes(data.data2);
					this.userData.removeProcess(data.id);
					this.userData.syncProfileData(['processes', 'bytes']);
				break;
				
				//Keygen upgrade end
				case 'end_keygen':
					console.log('socket - structure - '+event);
				
					let keys = [0,0,0,0];
					keys[data.data2-1] = 1;
						
					this.userData.addKeys(keys, false);
					this.userData.removeProcess(data.id);
					this.userData.syncProfileData(['processes', 'keys']);
				break;
				
				//Decrypter upgrade end
				case 'end_decrypter':
					console.log('socket - structure - '+event);
					
					this.userData.addShards(data.data2);
					this.userData.removeProcess(data.id);
					this.userData.syncProfileData(['processes', 'shards']);
				break;
			}
		}.bind(this));
	}
	
	addSite(site){
		var url = new URL(site);
		
		//If it already exist i remove it before adding it again
		var index = this.last_sites.findIndex(function(site){ return site.url == url.host; });
		if(index !== -1)
			this.last_sites.splice(index, 1);
		
		//Adding the site
		this.last_sites.push({
			url : url.host,
			timestamp : new Date().getTime()
		});
		
		//Removing older entries if more than 5
		if(this.last_sites.length > 5)
			this.last_sites.shift();
		
		this.syncNodesData('last_sites', this.last_sites);
	}
	
	//Remove a Vairus assigned to a node
	removeAssignment(vairus_to_remove){
		this.nodes_data.forEach(function(node){
			if(node.structures[0].hasOwnProperty('vairus') && vairus_to_remove == node.structures[0].vairus){
				node.structures[0].vairus = null;

				this.syncNodesData('assign_vairus', {
					node_id : node.id,
					assignment : null,
				});
			}
		});
	}
	
	//Syncronization of the network data with the profile pages
	syncNodesData(type, data){
		browser.tabs.query({}, function (tabs) {
			tabs.forEach(function(tab){
				if(tab.url == browser.runtime.getURL('/profile/profile.html')){
					browser.tabs.sendMessage(tab.id, {
						type : 'sync_nodes',
						data : {
							type : type,
							data : data
						}
					});
				}
			}.bind(this));
		}.bind(this));
	}
	
	

	//Send a notification to the active tab
	sendActiveTabNotification(message, color = 'azure'){
		browser.tabs.query({active: true, currentWindow: true}, function(tabs){
			tabs.forEach(function(tab){
				browser.tabs.sendMessage(tab.id, {
					type : 'generic_notification',
					data : {
						message : message,
						color : color
					}
				});
			}.bind(this));
		});
	}
	
	isSocketConnected(){
		return (typeof this.socket !== 'undefined' && this.socket.connected);
	}
}