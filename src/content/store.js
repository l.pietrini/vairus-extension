import Vue from 'vue'
import Vuex from 'vuex'
import seedrandom from 'seedrandom';

Vue.use(Vuex)

var browser = browser || chrome;

export default new Vuex.Store({
	state: {
		tutorial : [],
		current_tutorial : {
			active : false,
			flag : '',
			fast : false
		},
		tutorial_color : 'azure',
		tooltip : {
			text: '',
			top: 0,
			left: 0
		},
		options : {}
	},
	mutations: {
		setOptions(state, data){
			state.options = data;
		},
		setTooltip(state, data){
			state.tooltip.text = data.text;
			state.tooltip.top = data.event.clientY-30;
			state.tooltip.left = data.event.clientX+10;
		},
		closeTooltip(state){
			state.tooltip.text = '';
		},
		setTutorial(state, data){
			state.current_tutorial.active = true;
			state.current_tutorial.flag = data.flag;
			state.current_tutorial.fast = data.fast;
			
			if(data.update){
				browser.runtime.sendMessage({
					type : 'tutorial_flag',
					data : data.flag
				});
			}
			if(data.color){
				state.tutorial_color = data.color;
			}
		},
		closeTutorial(state){
			state.current_tutorial.active = false;
			state.current_tutorial.flag = '';
		},
	}
})