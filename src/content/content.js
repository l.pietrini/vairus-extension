var browser = browser || chrome;

import Vue from 'vue';
import App from './App';
import store from './store'

//Check if we're connected to the server
chrome.runtime.sendMessage({type : 'check_login_status'}, function(response) {
	if(response.active && response.has_data && response.status == 'logged_in'){	
		var options = response.options;
		var tutorial = response.tutorial;
		
		//Tooltip	
		Vue.directive('tooltip', {
			bind: function(el, binding, vnode){
				el.__setTooltip = function(e){
					vnode.context.$store.commit('setTooltip', {
						text : browser.i18n.getMessage(binding.value),
						event : e
					});
				}
				el.__closeTooltip = function(e){
					vnode.context.$store.commit('closeTooltip');
				}
				
				el.addEventListener('mouseenter', el.__setTooltip);
				el.addEventListener('mouseleave', el.__closeTooltip);
			},
			unbind: function(el){
				el.removeEventListener('mouseenter', el.__setTooltip);
				el.removeEventListener('mouseleave', el.__closeTooltip);
				
				delete el.__setTooltip;
				delete el.__closeTooltip;
			}
		})
		
		Vue.directive('tooltip2', {
			bind: function(el, binding, vnode){
				el.__setTooltip = function(e){
					vnode.context.$store.commit('setTooltip', {
						text : binding.value,
						event : e
					});
				}
				el.__closeTooltip = function(e){
					vnode.context.$store.commit('closeTooltip');
				}
				
				el.addEventListener('mouseenter', el.__setTooltip);
				el.addEventListener('mouseleave', el.__closeTooltip);
			},
			unbind: function(el){
				el.removeEventListener('mouseenter', el.__setTooltip);
				el.removeEventListener('mouseleave', el.__closeTooltip);
				
				delete el.__setTooltip;
				delete el.__closeTooltip;
			}
		})
		
		//Loading the fonts
		var styleNode = document.createElement ("style");
		styleNode.type = "text/css";
		styleNode.textContent = "@font-face { font-family: 'ChakraPetch'; src: url('" + chrome.extension.getURL ("../fonts/ChakraPetch-Regular.ttf") + "'); }" ;
		document.head.appendChild (styleNode);

		var styleNode = document.createElement ("style");
		styleNode.type = "text/css";
		styleNode.textContent = "@font-face { font-family: 'ChakraPetch'; src: url('" + chrome.extension.getURL ("../fonts/ChakraPetch-Bold.ttf") + "'); font-weight: bold; }" ;
		document.head.appendChild (styleNode);

		styleNode = document.createElement ("style");
		styleNode.type = "text/css";
		styleNode.textContent = "@font-face { font-family: 'The Hustle'; src: url('" + chrome.extension.getURL ("../fonts/The Hustle.ttf") + "'); }" ;
		document.head.appendChild (styleNode);
		
		fetch(chrome.runtime.getURL('content/content.html'))
			.then(function(response) {  return response.text() })
			.then(function(data) {   
				var shadow_div = document.createElement('div');
				shadow_div.id = 'vairus-root';
				shadow_div.attachShadow({mode: "open"});
				shadow_div.shadowRoot.innerHTML = data;
				
				shadow_div.style["position"] = "absolute";
				shadow_div.style["width"] = "100%";
				shadow_div.style["top"] = "0";
				shadow_div.style["left"] = "0";
				
				/* LOAD STYLE */

				fetch(chrome.runtime.getURL('content/content.css'))
					.then(function(response) {  return response.text() })
					.then(function(data) { 
						var css = document.createElement('style');
						css.appendChild(document.createTextNode(data));
						shadow_div.shadowRoot.appendChild(css);
					});

				/* END LOAD */

				document.body.append(shadow_div);

				new Vue({
					el: shadow_div.shadowRoot.querySelector('#vairus-vue-app'),
					store: store,
					render: h => h(App, {props: {
						options: options,
						tutorial: tutorial,
					}})
				});
			});


	}
});


/* MESSAGE HANDLER */







	