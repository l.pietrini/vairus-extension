var browser = browser || chrome;
import structuresManager from '../classes/structuresManagerClass.js';
import crypto from 'crypto';

export const structuresMixin = {
	methods: {
		structureMaxLevel(structure){
			return structuresManager.structures[structure].max_level;
		},
		structureUpgradeCost(structure, level){
			return structuresManager.getLevelupCost(structure, level+1);
		},
		structureEffect(structure, level){
			return structuresManager.getEffect(structure, level);
		},
		structureStat(structure){
			return structuresManager.getStat(structure);
		},
		checkStructureByteCost(structure, level){
			let result = true;
			let cost = this.structureUpgradeCost(structure, level);
			return (cost > this.bytes);
		},
		findVairus(vairus_id){
			return this.vairus_list.find(function(vairus){ return vairus.unique_id == vairus_id; });
		},
		upgradeNode(node_id){
			this.$store.commit('setActionLock', 'upgrade_node_lock');
			
			browser.runtime.sendMessage({
				type : 'upgrade_node',
				data : { 
					node_id : node_id.replace('n', '')
				}
			});
		},
		getBiome(host){
			var md5 = crypto.createHash('md5');
			md5.update(host);
			var digest = md5.digest('hex')
			
			let biomes = ['mountain','ocean','desert','sky','swamp','city','glacier','forest'];
			let hash = Math.abs(this.hashCode(digest));
			let index = (hash%8);
			return biomes[index];
		},
		hashCode(string) {
			var hash = 0, i, chr;
			if (string.length === 0) return hash;
			for (i = 0; i < string.length; i++) {
				chr   = string.charCodeAt(i);
				hash  = ((hash << 5) - hash) + chr;
				hash |= 0;
			}
			return hash;
		},
	}
}