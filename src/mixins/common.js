var browser = browser || chrome;

import seedrandom from 'seedrandom';

export const commonMixin = {
	methods: {
		getLocale(msg, substitution = null){
			return browser.i18n.getMessage(msg, substitution);
		},
		nodeFavicon(host){
			return 'url(https://www.google.com/s2/favicons?sz=64&domain='+host+')';
		},
		getImage(image){
			return browser.runtime.getURL('/images/'+image);
		},
		getVairusImage(id, part){
			return 'url('+browser.runtime.getURL('/images/vairus/v'+id+'/'+part+'.png')+')';
		},
		getShardImage(type){
			return 'url('+browser.runtime.getURL('/images/shards/'+type+'.png')+')';
		},
		getKeyImage(type){
			return 'url('+browser.runtime.getURL('/images/key'+type+'.png')+')';
		},
		getStructureImage(type){
			return 'url('+browser.runtime.getURL('/images/structures/'+type+'.png')+')';
		},
		getBytesImage(){
			return 'url('+browser.runtime.getURL('/images/bytes_white.svg')+')';
		},
		checkTutorial(flag){
			let f = this.tutorial.find(function(f){
				return f.flag == flag;
			});
			
			if(typeof f === 'undefined') return true;
			else return f.value == 1;
		},
		polarToCartesian(centerX, centerY, radius, angleInDegrees) {
			var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

			return {
				x: centerX + (radius * Math.cos(angleInRadians)),
				y: centerY + (radius * Math.sin(angleInRadians))
			};
		},
		describeArc(startAngle, endAngle, radius, width){
			var start = this.polarToCartesian(radius+(width/2), radius+(width/2), radius, endAngle);
			var end = this.polarToCartesian(radius+(width/2), radius+(width/2), radius, startAngle);

			var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

			var d = [
				"M", start.x, start.y, 
				"A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
			].join(" ");

			return d;       
		},
	}
}