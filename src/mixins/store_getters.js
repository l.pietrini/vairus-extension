import vairusData from '../data/vairus.json';
import nodesData from '../data/nodes.json';
import raritiesManager from '../classes/raritiesManagerClass.js';
	
export const storeGettersMixin = {
	computed: {
		current_time() {
			return this.$store.state.current_time;
		},
		username() {
			return this.$store.state.username;
		},
		bytes() {
			return this.$store.state.bytes;
		},
		chips() {
			return this.$store.state.chips;
		},
		shards() {
			return this.$store.state.shards;
		},
		keys() {
			return this.$store.state.keys;
		},
		v_progress() {
			return this.$store.state.v_progress;
		},
		boxes() {
			return this.$store.state.boxes;
		},
		processes() {
			return this.$store.state.processes;
		},
		dailies_seed() {
			return this.$store.state.dailies_seed;
		},
		dailies_progress() {
			return this.$store.state.dailies_progress;
		},
		dailies_rewards() {
			return this.$store.state.dailies_rewards;
		},
		
		tutorial() {
			return this.$store.state.tutorial;
		},
		current_tutorial() {
			return this.$store.state.current_tutorial;
		},
		tutorial_color() {
			return this.$store.state.tutorial_color;
		},
		options() {
			return this.$store.state.options;
		},
		links() {
			return this.$store.state.links;
		},
		
		vairus_list() {
			return this.$store.state.vairus_list;
		},
		detailed_vairus() {
			return this.$store.state.detailed_vairus;
		},
		detailed_index(){
			return this.$store.state.vairus_list.findIndex(function(v){
				return v.unique_id == this.$store.state.detailed_vairus;
			}.bind(this));
		},
		nodes() {
			return this.$store.state.nodes;
		},
		current_day() {
			return this.$store.state.current_day;
		},
		current_date() {
			return this.$store.state.current_date;
		},
		matrix_info() {
			return this.$store.state.matrix_info;
		},
		matrix_rewards() {
			return this.$store.state.matrix_rewards;
		},
		
		milestones(){
			return this.$store.state.milestones;
		},
		info_fragments(){
			return this.$store.state.info_fragments;
		},
		fragments_changes(){
			return this.$store.state.fragments_changes;
		},
		analyzer(){
			return this.$store.state.analyzer;
		},
		probes_rewards(){
			return this.$store.state.probes_rewards;
		},
		analysis_count(){
			return this.$store.state.analysis_count;
		},
		analysis(){
			return this.$store.state.analysis;
		},
		
		flat_vairus_list(){
			let dex = vairusData.map(function(entry){
				entry.unlocked = false;
				entry.current_part = '';
				entry.catched = 0;
				for(let attribute in entry.affinities){
					entry.affinities[attribute] = false;
				}
				return entry;
			})
			
			this.vairus_list.forEach(function(vairus){
				dex[vairus.vairus_type.id-1].unlocked = true;
				dex[vairus.vairus_type.id-1].catched++;
				vairus.attributes.forEach(function(attribute){
					dex[vairus.vairus_type.id-1].affinities[attribute] = true;
				}.bind(this));
			}.bind(this));
		
			return dex;
		},
		grid_nodes(){
			let grid_nodes = [];
			nodesData.forEach(function(node){
				//Nodo sbloccato
				var n = this.nodes.find(function(n){ return n.id == node.id; });
				
				if(typeof n != 'undefined'){
					node.data = n;
				}else{
					node.data = null
				}
				
				//Nodo sbloccabile
				var parent = this.nodes.find(function(n){ 
					return n.id == node.parent; 
				});
				
				if(typeof parent != 'undefined' || node.parent == 'n0'){
					node.available = true;
				}else{
					node.available = false;
				}
				
				grid_nodes.push(node);
			}.bind(this));
			
			return grid_nodes
		},
		active_nodes(){
			return this.grid_nodes.filter(function(node){
				return(node.data != null);
			});
		},
		collection_score(){
			let score = 0;
			
			var rarities_scores = {};
			
			raritiesManager.list.forEach(function(rarity){
				rarities_scores[rarity.name] = rarity.id;
			});
			
			this.flat_vairus_list.forEach(function(vairus){
				if(vairus.unlocked){
					score += rarities_scores[vairus.rarity];	
					
					for(var attribute in vairus.affinities){
						if(vairus.affinities[attribute]){
							score += 1;
						}
					}
				}
			}.bind(this));
			
			return score;
		},
		network_score(){
			let score = 0;
			this.grid_nodes.forEach(function(node){
				if(node.data != null){
					if(node.type == 'builder') score+=3;
					else score+=5;
					
					score+= Math.floor(node.data.level/5);
				}
			});
			return score;
		},
		unlock_node_lock(){
			return this.$store.state.unlock_node_lock;
		},
		upgrade_node_lock(){
			return this.$store.state.upgrade_node_lock;
		},
		start_process_lock(){
			return this.$store.state.start_process_lock;
		},
		complete_daily_lock(){
			return this.$store.state.complete_daily_lock;
		},
		upgrade_analyzer_lock(){
			return this.$store.state.upgrade_analyzer_lock;
		},
		network_introduction(){
			return this.$store.state.network_introduction;
		}
	},
	methods: {
		setTutorial(flag, update = false, fast = false, color = 'azure'){
			this.$store.commit('setTutorial', {
				flag : flag,
				update : update,
				fast : fast,
				color : color
			});
		},
		closeTutorial(){
			this.$store.commit('closeTutorial');
		}
	},
}