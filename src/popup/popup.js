import Vue from 'vue';
import App from './App';

document.fonts.add(new FontFace("ChakraPetch", "url('../fonts/ChakraPetch-Bold.ttf')", {weight : 700}));
document.fonts.add(new FontFace("ChakraPetch", "url('../fonts/ChakraPetch-Regular.ttf')", {weight : 400}));
document.fonts.add(new FontFace("The Hustle", "url('../fonts/The Hustle.ttf')"));

var app = new Vue({
	el: '#app',
	render: h => h(App)
})

