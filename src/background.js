import io from 'socket.io-client';
import axios from 'axios';
import UserData from './background/userdata.js';
import CatchHandler from './background/catchhandler.js';
import StructureHandler from './background/structurehandler.js';
import MatrixHandler from './background/matrixhandler.js';
import AnalyzerHandler from './background/analyzerhandler.js';
import crypto from 'crypto';

var browser = browser || chrome;
var version = '0.13.4';
var version_valid = true;
var jwt;
var username = null;
var email = null;
var password = null;
var socket;
// var auth_server = 'http://localhost:3001';
// var socket_server = 'http://localhost:3000';
var auth_server = 'https://playvairus.com/auth';
var socket_server = 'https://playvairus.com/';
var socket_path =  '';
var reconnect = true;
var reconnect_timer;
var reconnect_delay = 5000;
var active = true;

var userData = new UserData();
var catchHandler = new CatchHandler(userData);
var structureHandler = new StructureHandler(userData);
var matrixHandler = new MatrixHandler(userData, structureHandler);
var analyzerHandler = new AnalyzerHandler(userData, structureHandler);

socket_path =  '/api';

browser.storage.local.get(['vairus_jwt','vairus_email','vairus_pass','vairus_active'], function(result) {
	if(result.hasOwnProperty('vairus_jwt')) jwt = result.vairus_jwt;
	if(result.hasOwnProperty('vairus_email')) email = result.vairus_email;
	if(result.hasOwnProperty('vairus_pass')) password = result.vairus_pass;
	if(result.hasOwnProperty('vairus_active')) active = result.vairus_active;
	
	if(active){
		//Context menù action
		browser.contextMenus.create({
			id: "analyze-website",
			title: browser.i18n.getMessage("analyze_website"),
			onclick : openAnalyer
		});
	}
	
	autoLogin();
});

/* AUTO LOGIN */

function autoLogin(){
	if(typeof jwt !== 'undefined'){
		console.log('local token');
		connectSocket(jwt);
		let decoded = parseJwt(jwt);
		username = decoded.username;
	}else{
		console.log('no local token');
		
		if(typeof email !== 'undefined' && typeof password !== 'undefined'){
			console.log('local login data');
			requestNewToken(email, password);
		}else{
			console.log('no local login data');
		}
	}
}

/* LOGIN HANDLER */

var login_lock = false;
function requestNewToken(email, pass, sendResponse = false){
	if(!login_lock && email != null && pass != null){
		console.log('req token');
		
		login_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('email', email);
		data.append('password', pass);
		
		axios.post( auth_server+'/login', data)
			.then(function (response) {
				if(response.data.success){
					console.log('got new token');
					
					jwt = response.data.message;
					let decoded = parseJwt(jwt);
					
					username = decoded.username;
					email = email;
					password = pass;
					
					browser.storage.local.set({
						vairus_jwt: jwt,
						vairus_email: email, 
						vairus_pass : pass
					});
					connectSocket(jwt, sendResponse);
					
				}else{
					if(sendResponse){
						sendResponse({
							success: false,
							message : response.data.message
						});
					}
				}
				login_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				sendResponse({
					success: false,
					message : 'Server offline'
				});
				login_lock = false;
			});
	}
}

function parseJwt(token){
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
}

/* REGISTER HANDLER */

var register_lock = false;
function registerNewUser(username, password, email, newsletter, sendResponse = false){
	console.log(username, password, email);
	if(!register_lock){
		console.log('register user');
		
		register_lock = true;
		axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

		var data = new URLSearchParams();
		data.append('username', username);
		data.append('password', password);
		data.append('email', email);
		data.append('newsletter', newsletter);
		
		axios.post( auth_server+'/register', data)
			.then(function (response) {
				if(response.data.success){
					requestNewToken(email, password, sendResponse);
				}else{
					if(sendResponse){
						sendResponse({
							success: false,
							message : response.data.message
						});
					}
				}
				register_lock = false;
			})
			.catch(function (error) {
				console.log(error);
				sendResponse({
					success: false,
					message : 'Server offline'
				});
				register_lock = false;
			});
	}	
}

/* SOCKET HANDLER */

function connectSocket(jwt, sendResponse = false){
	if(!isSocketConnected()){
		console.log('connecting socket');
			
		let args = {
			query : 'token=' + jwt,
			reconnection : false,
			transports: ['websocket'], 
			upgrade: false
		};
		
		if(socket_path != ''){
			args.path = socket_path;
		}
			
		socket = io.connect(socket_server, args);
		reconnect = true;
		
		/* CONNECTION HANDLER */
		
		socket.on('connect', (data) => {
			if(sendResponse){
				sendResponse({
					success: true,
					message : username
				});
			}
			
			userData.updateSocket(socket, username);
			catchHandler.updateSocket(socket);
			structureHandler.updateSocket(socket);
			matrixHandler.updateSocket(socket);
			analyzerHandler.updateSocket(socket);
			
			socket.emit('check_version',{ version : version });
		
			//Reset the current reconnection timer and set the timer to the minimum value
			clearTimeout(reconnect_timer);
			reconnect_delay = 4000 +Math.floor(Math.random()*2000);
			changeFavicon();
			
		});
		
		//On disconnection i'll try to reconnect after a delay
		socket.on('disconnect', (data) => {
			console.log('socket - disconnect');
			
			clearTimeout(reconnect_timer);
			
			if(reconnect){
				reconnect_timer = setTimeout(function(){
					console.log('socket - reconnect');
					connectSocket(jwt);
				}, reconnect_delay);
			}
			changeFavicon();
		});
		
		//On connection failed i'll try to reconnect with an increasing delay
		socket.on('connect_error', (data) => {
			console.log('socket - connect_error');
			console.log('retry with delay '+reconnect_delay);
			
			clearTimeout(reconnect_timer);
			if(reconnect){
				if(reconnect_delay > 60000){
					userData.has_data = false;
				}
				
				reconnect_timer = setTimeout(function(){
					console.log('socket - reconnect');
					connectSocket(jwt);
				}, reconnect_delay);
			}
			
			reconnect_delay = reconnect_delay*2;
			if(reconnect_delay > 300000){
				reconnect_delay = 240000 +Math.floor(Math.random()*120000);
			}
			
			if(sendResponse){
				sendResponse({
					success: false,
					message : 'Server offline'
				});
			}
		});
		
		socket.on('connect_failed', (data) => {
			console.log('socket - connect_failed');
		});
		
		//On connection error i request the server for a new token
		socket.on('error', (data) => {
			console.log('socket - error');
			
			if(data.code == 'invalid_token'){
				console.log('token expired');
			
				if(typeof email !== 'undefined' && typeof password !== 'undefined'){
					console.log('local login data');
					requestNewToken(email, password);
				}else{
					console.log('no local login data');
				}
			}
			
			if(sendResponse){
				sendResponse({
					success: false,
					message : 'Socket error'
				});
			}
		});
		
		/* SERVER MESSAGE HANDLER */
		
		
		//Event handler for the socket
		var onevent = socket.onevent;
		socket.onevent = function (packet) {
			var args = packet.data || [];
			onevent.call (this, packet);
			packet.data = ["*"].concat(args);
			onevent.call(this, packet); 
		};
		
		socket.on("*", function(event, data) {
			
			switch(event){
				//FORCE DISCONNECT
				case 'force_disconnect':
					reconnect = false;
				break;
				
				//VERSION CHECK
				case 'check_version_response':
					console.log('socket - '+event);
					
					if(data){
						socket.emit('get_userdata');
					}else{
						reconnect = false;
						version_valid = false;
						userData.has_data = false;
						
						socket.disconnect();
						
						browser.runtime.sendMessage({
							type : 'version_not_valid',
						});
					}
				break;
									
				//EXHAUST VAIRUS
				case 'exhaust_vairus':
					console.log('socket - '+event);
					
					userData.updateVairusExhaustion(data.vairus_id, data.exhaustion);
				break;
				
				//MUTATION RESULT
				case 'mutation_result':
					console.log('socket - '+event);
					if(data.success){
						//Update the mutated Vairus
						let vairus = userData.vairus_list.find(function(v){
							return v.unique_id == data.message.mutation.unique_id;
						});
						
						vairus.updateAttributes(data.message.mutation.attributes);
						userData.shards[data.message.shards.type] -= data.message.shards.cost;
						
						userData.syncVairusData('update_vairus_attributes', {
							unique_id : data.message.mutation.unique_id,
							attributes : data.message.mutation.attributes,
							new_stats : vairus.current_stats,
							stats_array : vairus.stats_array,
						});
						
						userData.dailies_progress.push({
							type : 'mutation',
							subtype : '',
							date : Math.floor((new Date()).getTime() / 1000)
						});
						userData.syncProfileData(['shards', 'dailies_progress']);
					}
					
					mutationResponse(data);
				break;
				
				//POWERUP RESULT
				case 'powerup_result':
					console.log('socket - '+event);
					if(data.success){
						let vairus = userData.vairus_list.find(function(v){
							return v.unique_id == data.message.vairus_id;
						});
						
						let date = Math.floor((new Date()).getTime() / 1000);
						let level_to_add = data.message.newlevel - vairus.level;
						
						vairus.updateLevel(data.message.newlevel);
						userData.addBytes(-1*data.message.cost);
						userData.addKeys(data.message.keys, true);
						
						userData.syncVairusData('update_vairus_level', {
							unique_id : data.message.vairus_id,
							new_level : data.message.newlevel,
							new_stats : vairus.current_stats,
							stats_array : vairus.stats_array,
						});
						
						userData.dailies_progress.push({
							type : 'powerup',
							subtype : level_to_add,
							date : date
						});
						userData.syncProfileData(['bytes', 'keys', 'dailies_progress']);
					}
					
					powerupResponse(data);
				break;
				
				//DESTROY RESULT
				case 'destroy_result':
					console.log('socket - '+event);
					if(data.success){
						vairus_to_destroy.forEach(function(vairus){
							var index = userData.vairus_list.findIndex(function(v){
								return v.unique_id == vairus;
							});
							
							var exhaustion = userData.vairus_list[index].exhaustion;
							
							if(exhaustion != 'assigned' && exhaustion != 'matrix'){
								userData.vairus_list.splice(index, 1);
								structureHandler.removeAssignment(vairus);
							}
						});
						userData.addBytes(data.message.bytes);
						
						userData.syncVairusData('destroy_vairus', {
							vairus_to_destroy : vairus_to_destroy,
						});
						userData.syncProfileData(['bytes']);
					}
					destroyResponse(data);
				break;
				
			}
		});
	}
}

function isSocketConnected(){
	return (typeof socket !== 'undefined' && socket.connected);
}

/* EXTENSION MESSAGE HANDLER */

var mutationResponse = false;
var powerupResponse = false;
var destroyResponse = false;

var vairus_to_destroy;

browser.runtime.onMessage.addListener( function(request, sender, sendResponse) {
	
	switch(request.type){
		//FROM POPUP - Check if i'm connected to the server
		case 'check_login_status':
			console.log('extensions - '+request.type);
			
			var status = 'not_logged_in';
			
			if(isSocketConnected()){
				status = 'logged_in';
			}else if(email != null && password != null){
				status = 'reconnect';
			}
			
			var data = {
				status : status,
				active : active,
				has_data : userData.has_data,
				username : username,
				options : userData.options,
				tutorial : userData.tutorial,
			};
			
			sendResponse( data );
		break;
		
		
		//FROM POPUP - Request a token for server connection
		case 'login':		
			console.log('extensions - '+request.type);
			requestNewToken(request.data.email, request.data.password, sendResponse);
			return true;
		
		
		//FROM POPUP - Request a reconnection
		case 'reconnect':		
			console.log('extensions - '+request.type);
			requestNewToken(email, password, sendResponse);
			return true;
		
		
		//FROM POPUP - Request the registration of a new user
		case 'register':
			console.log('extensions - '+request.type);
			registerNewUser(request.data.username, request.data.password, request.data.email, request.data.newsletter, sendResponse);
			return true;
		
		//FROM POPUP - Toggle Vairus active
		case 'activate':
			console.log('extensions - '+request.type);
			active = request.active;
			browser.storage.local.set({ vairus_active: active });
			
			//Context menù action
			if(active){
				browser.contextMenus.create({
					id: "analyze-website",
					title: browser.i18n.getMessage("analyze_website"),
					onclick : openAnalyer
				});
			}else{
				browser.contextMenus.remove("analyze-website");
			}
			
			changeFavicon();
		break;
		
		//FROM PROFILE - Request the user data from the server
		case 'refresh_userdata':
			console.log('extensions - '+request.type);
			socket.emit('get_userdata');
		break;
		
		//FROM CONTENT - Navigate to a website
		case 'navigate_url':
			var url = new URL(request.data.url);
			var md5 = crypto.createHash('md5');
			md5.update(url.host);
			var digest = md5.digest('hex');
			
			if(isSocketConnected() && active){
				socket.emit('navigate_url', {
					tab_id : sender.tab.id,
					url : digest
				});
			}
			
			structureHandler.addSite(request.data.url);
		break;
		
		//FROM PROFILE - Rename a Vairus
		case 'edit_label':
			console.log('extensions - '+request.type);
			socket.emit('edit_label', request.data);

			let vairus = userData.vairus_list.find(function(v){
				return v.unique_id == request.data.unique_id;
			});
			vairus.label = request.data.label;
			
			userData.syncVairusData('update_vairus_label', {
				unique_id : request.data.unique_id,
				label : request.data.label,
			});
		break;
		
		
		//FROM PROFILE - Mutate a Vairus
		case 'mutate_vairus':
			console.log('extensions - '+request.type);
			socket.emit('mutate_vairus', request.data);
			mutationResponse = sendResponse;
			return true;
		
		
		//FROM PROFILE - Powering up a Vairus
		case 'powerup_vairus':
			console.log('extensions - '+request.type);
			socket.emit('powerup_vairus', request.data);
			powerupResponse = sendResponse;
			return true;
		
		
		//FROM PROFILE - Destroying a list of Vairus
		case 'destroy_vairus':
			console.log('extensions - '+request.type);
			socket.emit('destroy_vairus', request.data);
			destroyResponse = sendResponse;
			vairus_to_destroy = request.data.list;
			return true;
		
		
		//FROM PROFILE - Send feedback
		case 'send_feedback':
			console.log('extensions - '+request.type);
			socket.emit('send_feedback', request.data);
		break;
	}
});

function openAnalyer(info, tab){
	var analysis = null;
	var url = new URL(info.pageUrl);
	var md5 = crypto.createHash('md5');
	md5.update(url.host);
	var digest = md5.digest('hex');
	
	//Recupero analisi
	if(analyzerHandler.analysis.hasOwnProperty(digest)){
		var date = new Date();
		var day = date.getUTCFullYear() +'-'+ date.getUTCMonth() +'-'+ date.getUTCDate();
		if(day == analyzerHandler.analysis[digest].day){
			analysis = analyzerHandler.analysis[digest];
		}
	}
	
	//Recupero sonde
	var probes = userData.processes.filter(function(p){
		return p.type == 'probe';
	});
	
	browser.tabs.sendMessage(tab.id, {
		type : 'analysis_request',
		data : analyzerHandler.analyzer,
		analysis : analysis,
		probes : probes,
	});
}

//Toggle the Vairus icon 
function changeFavicon(){
	var path = (isSocketConnected() && active) ? '../images/favicon_active.png' : '../images/favicon.png';
	browser.browserAction.setIcon({ path: path});
}

 