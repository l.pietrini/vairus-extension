var browser = browser || chrome;

import Vue from 'vue';
import App from './App';
import store from './store'

document.fonts.add(new FontFace("The Hustle", "url('../fonts/The Hustle.ttf')"));
document.fonts.add(new FontFace("Inconsolata", "url('../fonts/Inconsolata.ttf')"));
document.fonts.add(new FontFace("ChakraPetch", "url('../fonts/ChakraPetch-Bold.ttf')", {weight : 700}));
document.fonts.add(new FontFace("ChakraPetch", "url('../fonts/ChakraPetch-Regular.ttf')", {weight : 400}));


Vue.directive('tooltip', {
	bind: function(el, binding, vnode){
		el.__setTooltip = function(e){
			vnode.context.$store.commit('setTooltip', {
				text : browser.i18n.getMessage(binding.value),
				event : e
			});
		}
		el.__closeTooltip = function(e){
			vnode.context.$store.commit('closeTooltip');
		}
		
		el.addEventListener('mouseenter', el.__setTooltip);
		el.addEventListener('mouseleave', el.__closeTooltip);
	},
	unbind: function(el){
		el.removeEventListener('mouseenter', el.__setTooltip);
		el.removeEventListener('mouseleave', el.__closeTooltip);
		
		delete el.__setTooltip;
		delete el.__closeTooltip;
	}
})

Vue.directive('tooltip2', {
	bind: function(el, binding, vnode){
		el.__setTooltip = function(e){
			vnode.context.$store.commit('setTooltip', {
				text : binding.value,
				event : e
			});
		}
		el.__closeTooltip = function(e){
			vnode.context.$store.commit('closeTooltip');
		}
		
		el.addEventListener('mouseenter', el.__setTooltip);
		el.addEventListener('mouseleave', el.__closeTooltip);
	},
	unbind: function(el){
		el.removeEventListener('mouseenter', el.__setTooltip);
		el.removeEventListener('mouseleave', el.__closeTooltip);
		
		delete el.__setTooltip;
		delete el.__closeTooltip;
	}
})

new Vue({
	el: '#vairus-vue-app',
	store: store,
	render: h => h(App)
});