import Vue from 'vue'
import Vuex from 'vuex'
import seedrandom from 'seedrandom';

Vue.use(Vuex)

var browser = browser || chrome;

export default new Vuex.Store({
	state: {
		username : '',
		bytes : 0,
		chips : 0,
		shards : {},
		keys : [0,0,0,0],
		v_progress : [0,0,0,0],
		boxes : [],
		processes : [],
		dailies_seed : '',
		dailies_progress : [],
		dailies_rewards : [],
		
		tutorial : [],
		options : {},
		links : {},
		
		current_tutorial : {
			active : false,
			flag : '',
			fast : false
		},
		tutorial_color : 'azure',
		
		tooltip : {
			text: '',
			top: 0,
			left: 0
		},
		
		current_day : null,
		current_date : '',
		
		vairus_list : [],
		detailed_vairus : null,
		nodes : [],
		matrix_info : {},
		matrix_rewards : [],
		current_time : Date.now(),
		
		milestones : 0,
		info_fragments : {},
		fragments_changes : [],
		analyzer : null,
		probes_rewards : [],
		analysis_count : 0,
		analysis : {},
		
		//ACTION LOCKS
		unlock_node_lock : false,
		upgrade_node_lock : false,
		start_process_lock : false,
		complete_daily_lock : false,
		upgrade_analyzer_lock : false,
		
		//FLAGS
		network_introduction : false,
	},
	mutations: {
		refreshCurrentTime(state){
			state.current_time = Date.now();
		},
		setResource(state, data){
			state[data.type] = data.value;
			
			switch(data.type){
				case 'processes':
					state.start_process_lock = false;
				break;
				case 'dailies_rewards':
					state.complete_daily_lock = false;
				break;
			}
		},
		setDetailed(state, vairus){
			state.detailed_vairus = vairus;
		},
		setVairusList(state, vairus_list){
			state.vairus_list = vairus_list;
		},
		addVairus(state, vairus){
			state.vairus_list.push(vairus);
		},
		updateVairusExhaustion(state, data){
			let vairus = state.vairus_list.find(function(vairus){
				return vairus.unique_id == data.unique_id;
			});
			vairus.exhaustion = data.exhaustion;
		},
		updateVairusLevel(state, data){
			let vairus = state.vairus_list.find(function(vairus){
				return vairus.unique_id == data.unique_id;
			});
			vairus.level = data.new_level;
			vairus.current_stats = data.new_stats;
			vairus.stats_array = data.stats_array;
		},
		updateVairusAttributes(state, data){
			let vairus = state.vairus_list.find(function(vairus){
				return vairus.unique_id == data.unique_id;
			});
			vairus.attributes = data.attributes;
			vairus.current_stats = data.new_stats;
			vairus.stats_array = data.stats_array;
		},
		updateVairusLabel(state, label){
			let vairus = state.vairus_list.find(function(vairus){
				return vairus.unique_id == data.unique_id;
			});
			vairus.label = label;
		},
		destroyVairus(state, to_destroy){
			to_destroy.forEach(function(vairus){
				var index = state.vairus_list.findIndex(function(v){
					return v.unique_id == vairus;
				})
				
				var exhaustion = state.vairus_list[index].exhaustion;
				
				if(exhaustion != 'assigned' && exhaustion != 'matrix'){
					state.vairus_list.splice(index, 1);
				}
			});
		},
		setNodes(state, nodes){
			state.nodes = nodes;
		},
		setNodeLastCatch(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			node.last_catch = data.last_catch;
		},
		unlockNode(state, node){
			state.nodes.push(node);
			state.unlock_node_lock = false;
		},
		setNodeHost(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			node.host = data.host;
			node.last_change = data.last_change;
			node.last_catch = data.last_catch;
		},
		upgradeNode(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			node.level = data.level;
			state.upgrade_node_lock = false;
		},
		upgradeStructure(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			let structure = node.structures.find(function(structure){ 
				return structure.id == data.structure_id; 
			});
			structure.level = data.level;
		},
		assignVairus(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			node.assignment = data.assignment;
		},
		nodeProgress(state, data){
			let node = state.nodes.find(function(node){
				return node.id == data.node_id;
			});
			node.progress = data.progress;
		},
		setMatrixRewards(state, rewards){
			state.matrix_rewards = rewards;
		},
		analyzerUpdate(state, data){
			state.analyzer = data;
			state.upgrade_analyzer_lock = false;
		},
		setTooltip(state, data){
			state.tooltip.text = data.text;
			state.tooltip.top = data.event.clientY-30;
			state.tooltip.left = data.event.clientX+10;
		},
		closeTooltip(state){
			state.tooltip.text = '';
		},
		setTutorial(state, data){
			state.current_tutorial.active = true;
			state.current_tutorial.flag = data.flag;
			state.current_tutorial.fast = data.fast;
			
			if(data.update){
				browser.runtime.sendMessage({
					type : 'tutorial_flag',
					data : data.flag
				});
			}
			if(data.color){
				state.tutorial_color = data.color;
			}
		},
		closeTutorial(state){
			state.current_tutorial.active = false;
			state.current_tutorial.flag = '';
		},
		setCurrentDay(state){
			let date = new Date();
			state.current_day = date.getUTCDate();
			state.current_date = date.getUTCFullYear() +'-'+ date.getUTCMonth() +'-'+ date.getUTCDate();
		},
		setMatrixInfo(state){
			//Calculate matrix level and time for the Network
			let date = new Date();
			let utc_day = date.getUTCDate();
			let utc_month = date.getUTCMonth()+1;
			let utc_year = date.getUTCFullYear();
			
			state.nodes.forEach(function(node){
				var seed = utc_day+'-'+utc_month+'-'+utc_year+node.hash;
				var rng = seedrandom(seed);
				
				var matrix_time = Math.floor(rng()*24);
				var matrix_level = Math.floor((rng()*4)+1);
				
				var matrix_date = new Date(utc_month+'/'+utc_day+'/'+utc_year+'/'+' '+matrix_time+':00 UTC');
				
				Vue.set(state.matrix_info, node.host, {
					level : matrix_level,
					time : matrix_date.getHours().toString().padStart(2, '0')+':'+matrix_date.getMinutes().toString().padStart(2, '0'),
				});
			});
		},
		setNetworkIntroduction(state){
			state.network_introduction = true;
		},
		setActionLock(state, lock){
			state[lock] = true;
		},
		resetLocks(state){
			state.unlock_node_lock = false;
			state.upgrade_node_lock = false;
			state.start_process_lock = false;
			state.complete_daily_lock = false;
		},
	},
	actions: {
		setCurrentDay(context) {
			if(context.current_day != new Date().getUTCDate()){
				setTimeout(function(){
					context.commit('setCurrentDay');
					context.commit('setMatrixInfo');
				}, 500);
			}
		}
	}
})